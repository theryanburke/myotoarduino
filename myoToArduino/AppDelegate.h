//
//  AppDelegate.h
//  myoToArduino
//
//  Created by Ryan Burke on 10/25/14.
//  Copyright (c) 2014 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

