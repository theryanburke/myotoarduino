//
//  main.m
//  myoToArduino
//
//  Created by Ryan Burke on 10/25/14.
//  Copyright (c) 2014 2lemetry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
